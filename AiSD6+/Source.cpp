#include <iostream>
#include <array>
#include <vector>
#include <algorithm>



std::array<int, 256> MakeDelta1(std::string pattern) 
{
	std::array<int, 256> delta1;

	for (auto &element : delta1) 
		element = pattern.length();
	for (int i = 0; i < pattern.length(); ++i)
	{
		delta1[pattern[i]] = pattern.length() - 1 - i;
	}

	return delta1;
}

bool IsPrefix(std::string word, int index) 
{
	for (int i = 0; i < word.length() - index; i++)
		if (word[i] != word[index + i])
			return false;
	return true;
}

int SuffixLength(std::string word, int index) 
{
	int length = 0;
	while ((word[index - length] == word[word.length() - 1 - length]) && (length < index))
		++length;
	return length;
}

std::vector<int> MakeDelta2(std::string pattern)
{
	std::vector<int> delta2(pattern.length());
	int last_prefix_index = pattern.length() - 1;

	for (int i = pattern.length() - 1; i >= 0; i--) 
	{
		if (IsPrefix(pattern, i + 1)) 
			last_prefix_index = i + 1;
		delta2[i] = last_prefix_index + (pattern.length() - 1 - i);
	}

	for (int i = 0; i < pattern.length() - 1; i++) 
	{
		int suffix_length = SuffixLength(pattern, i);

		if (pattern[i - suffix_length] != pattern[pattern.length() - 1 - suffix_length]) 
			delta2[pattern.length() - 1 - suffix_length] = pattern.length() - 1 - i + suffix_length;
	}

	return delta2;
}

int BoyerMoore(std::string text, std::string pattern) 
{
	if (pattern.length() == 0) return -1;

	int i;
	std::array<int, 256> delta1;
	std::vector<int> delta2;
	delta1 = MakeDelta1(pattern);
	delta2 = MakeDelta2(pattern);

	i = pattern.length() - 1;
	while (i < text.length()) 
	{
		int j = pattern.length() - 1;
		while (j >= 0 && (text[i] == pattern[j])) 
		{
			--i;
			--j;
		}
		if (j < 0)
			return i + 1;
		i += std::max(delta1[text[i]], delta2[j]);
	}
	return -1;
}

int main()
{
	std::string text = { "ABCABCABC" };
	std::string pattern = { "ABC" };

	int pos = BoyerMoore(text, pattern);
	if (pos != -1)
		std::cout << "znaleziono na pozycji: " << pos << "\n";
	else
		std::cout << "nie znaleziono\n";

	system("pause");
}